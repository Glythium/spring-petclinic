# Lab Exercise

### Goal
Set up a CI/CD pipeline for this repository.

### Troubleshooting
Ask a facilitator for help if needed.

### Setup
- Clone this repository to your local system
- Create a new branch for yourself and work out of that
- Register a runner with the new repository, you can activate your runner from yesterday for this project

### Exercise
1. Begin by crafting a `build` Job. Luckily, the developers left a [README](readme.md) in this repository with instructions on how they run their application locally. 

**Note**: We don't want to try and run the application from the pipeline, so let's focus on just trying to build the executable `.jar` file(s) for this step.

2. Now that we have the executable(s) getting built, we need to pass them on to a new Job that will `containerize` the application. Add a key to the `build` Job's definition that will pass the folder where the [build artifacts](https://docs.gitlab.com/ee/ci/yaml/#artifactspaths) are stored on through the pipeline.

3. Continue on to the [Containerize Job Exercise](02-Containerize-Job-Exercise.md)