# Lab Exercise

### Goal
Set up a CI/CD pipeline for this repository.

### Troubleshooting
Ask a facilitator for help if needed.

### Setup
- Complete the [Build Job Exercise](./01-Build-Job-Exercise.md)

### Exercise
1. Let's start building out that `containerize` Job. Use [the docs](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html#building-a-docker-image-with-kaniko) and then add keywords to [fetch the dependencies](https://docs.gitlab.com/ee/ci/yaml/#dependencies).

**Note**: If you're having the `containerize` Job run in the same Stage as the `build` Job, you'll also need to use [the needs keyword](https://docs.gitlab.com/ee/ci/yaml/#needs) to have the Jobs run in a specific order.

2. Continue on to the [Scan Stage Exercise](03-Scan-Stage-Exercise.md)