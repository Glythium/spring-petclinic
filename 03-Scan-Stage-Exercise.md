# Lab Exercise

### Goal
Set up a CI/CD pipeline for this repository.

### Troubleshooting
Ask a facilitator for help if needed.

### Setup
- Complete the [Containerize Job Exercise](02-Containerize-Job-Exercise.md)

### Exercise
1. We have a container being stored! Coincidentally, security wants us to integrate their new container scanning tool [Trivy](https://aquasecurity.github.io/trivy/v0.18.3/integrations/gitlab-ci/#gitlab-ci-using-trivy-container). They are ok with us just having the scan results get shown in the pipeline, we don't need to store those results anywhere else...yet.

2. Continue on to the [Deploy Stage Exercise](04-Deploy-Stage-Exercise.md)