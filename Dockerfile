FROM openjdk:17-slim-bullseye

# We're going to be root for this phase
USER root

# Update all the packages on the system to get all the good security stuff
RUN apt-get update
RUN apt-get upgrade -y

# Make our working directory and throw our .jar in there
RUN mkdir -p /petclinic
COPY ./target/*.jar /petclinic

# Prepare the working directory for our non-root user
RUN useradd java
RUN chown -R java /petclinic
WORKDIR /petclinic

# Now we're going to continue the rest as the new user
USER java
CMD java -jar *.jar