# Lab Exercise

### Goal
Set up a CI/CD pipeline for this repository.

### Troubleshooting
Ask a facilitator for help if needed.

### Setup
- Clone this repository to your local system
- Create a new branch for yourself and work out of that

### Exercise
1. Begin by crafting a `build` Job. Luckily, the developers left a [README](readme.md) in this repository with instructions on how they run their application locally. 

**Note**: We don't want to try and run the application from the pipeline, so let's focus on just trying to build the executable `.jar` file(s) for this step.

2. Now that we have the executable(s) getting built, we need to pass them on to a new Job that will `containerize` the application. Add a key to the `build` Job's definition that will pass the folder where the [build artifacts](https://docs.gitlab.com/ee/ci/yaml/#artifactspaths) are stored on through the pipeline.

3. Let's start building out that `containerize` Job. Take everything but the `script` block from [the docs](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html#building-a-docker-image-with-kaniko) and then add keywords to [fetch the dependencies](https://docs.gitlab.com/ee/ci/yaml/#dependencies).

**Note**: If you're having the `containerize` Job run in the same Stage as the `build` Job, you'll also need to use [the needs keyword](https://docs.gitlab.com/ee/ci/yaml/#needs) to have the Jobs run in a specific order.

4. Once we have the meta configuration for this Job figured out, ask the senior engineer for help with the script block. We're going to be authenticating with an external container registry, so this is going to introduce masked CI/CD variables.

5. We have a container being stored! Coincidentally, security wants us to integrate their new container scanning tool [Trivy](https://aquasecurity.github.io/trivy/v0.18.3/integrations/gitlab-ci/#gitlab-ci-using-trivy-container). They are ok with us just having the scan results get shown in the pipeline, we don't need to store those results anywhere else...yet.